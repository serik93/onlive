<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/normalize.css',
        'css/magnific-popup.css',
        'css/fontawesome-stars.css',
        'css/style.css',
        'css/font-awesome.min.css',
    ];
    public $js = [
        'js/plugins.js',
        'js/scripts.js'
    ];
    public $depends = [
    //    'yii\web\YiiAsset',
    //    'yii\bootstrap\BootstrapAsset',
    ];
}
