<?php

namespace frontend\controllers;

use common\models\Page;
use Yii;
use common\models\News;
use frontend\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['created_at' => SORT_DESC]
        ];

        $page = Page::find()->where(['slug' => 'news'])->one();
        if ($page) {
            $arrLangs = ['ru-RU' => 'ru', 'en-US' => 'en'];
            \notgosu\yii2\modules\metaTag\components\MetaTagRegister::register($page, $arrLangs[Yii::$app->language]);
        }

        return $this->render('index', ['dataProvider'=>$dataProvider]);
    }

    /**
     * Displays a single Webcam model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($slug)
    {
        $model = News::find()->andWhere(['slug'=>$slug])->one();

        //$latest = News::find()->orderBy(new Expression('updated_at'))->limit(3)->all();
        $searchModel = new NewsSearch();
        $latest = $searchModel->search(Yii::$app->request->queryParams);
        $latest->pagination = ['pageSize'=>3];
        $latest->sort = [
            'defaultOrder' => ['updated_at' => SORT_DESC]
        ];

        if (!$model) {
            throw new NotFoundHttpException;
        }

        $arrLangs = ['ru-RU'=>'ru', 'en-US'=>'en'];
        \notgosu\yii2\modules\metaTag\components\MetaTagRegister::register($model, $arrLangs[Yii::$app->language]);

        return $this->render('view', ['model'=>$model, 'latest' => $latest]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
