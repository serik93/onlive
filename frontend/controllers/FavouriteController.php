<?php

namespace frontend\controllers;

use Yii;
use common\models\Favourite;
use frontend\models\FavouriteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FavouriteController implements the CRUD actions for Favourite model.
 */
class FavouriteController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Favourite models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest)
            $this->redirect('/user/sign-in/login');
        $searchModel = new FavouriteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Favourite model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Favourite model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Favourite();
        if (Yii::$app->user->isGuest){ return -1;}
        elseif ($model->load(Yii::$app->request->post()) && $model->save()) {
            return 1;
        } else {
            return var_dump($model->getErrors());
        }
    }

    /**
     * Updates an existing Favourite model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionDeleteByWebcam(){
        $fav = Favourite::find()->where(['webcam_id'=>Yii::$app->request->post()['webcam_id'],'user_id'=>Yii::$app->user->id])->one();
        //return var_dump($fav);
        $fav->delete();
        return 1;
        //return var_dump(Yii::$app->request->post()['webcam_id']);
    }
    /**
     * Deletes an existing Favourite model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete()
    {
        $this->findModel(Yii::$app->request->post()['id'])->delete();
        return 1;
        //return $this->redirect(['index']);
    }

    /**
     * Finds the Favourite model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Favourite the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Favourite::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
