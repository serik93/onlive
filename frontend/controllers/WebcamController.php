<?php

namespace frontend\controllers;

use Yii;
use common\models\Webcam;
use common\models\City;
use common\models\WebcamCategory;

use frontend\models\WebcamSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use yii\data\Pagination;

/**
 * WebcamController implements the CRUD actions for Webcam model.
 */
class WebcamController extends Controller
{
    /**
     * @inheritdoc
     */
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new WebcamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', ['dataProvider'=>$dataProvider]);
    }

    /**
     * @return string
     */
    public function actionCity($city = 'Almaty', $category = '')
    {
        $searchModel = new WebcamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $dataProvider->sort = [
            'defaultOrder' => ['id' => SORT_DESC]
        ];

        $city = City::find()->where(['name_en'=>$city])->one();
        $arrLangs = ['ru-RU'=>'ru', 'en-US'=>'en'];
        \notgosu\yii2\modules\metaTag\components\MetaTagRegister::register($city, $arrLangs[Yii::$app->language]);

        return $this->render('city', ['dataProvider'=>$dataProvider, 'city'=>$city]);
    }
    /**
     * @return string
     */
    public function actionCategory($category)
    {
        $searchModel = new WebcamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['id' => SORT_DESC]
        ];
        return $this->render('category', ['dataProvider'=>$dataProvider, 'category'=>WebcamCategory::find()->where(['slug'=>$category])->one()]);
    }

    /**
     * @return string
     */
    public function actionPopular()
    {
        $searchModel = new WebcamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['viewed' => SORT_DESC]
        ];
        $this->view->params['breadcrumbs'] = [Yii::t('frontend','popular')];
        return $this->render('popular', ['dataProvider'=>$dataProvider]);
    }
    /**
     * @return string
     */
    public function actionLatest()
    {
        $searchModel = new WebcamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = [
            'defaultOrder' => ['updated_at' => SORT_DESC]
        ];
        $this->view->params['breadcrumbs'] = [Yii::t('frontend','latest')];
        return $this->render('latest', ['dataProvider'=>$dataProvider]);
    }

    /**
     * Displays a single Webcam model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($slug)
    {
        $ratings = null;

        $model = Webcam::find()->andWhere(['slug'=>$slug])->one();

        if (!$model) {
            throw new NotFoundHttpException;
        }
        $model->updateCounters(['viewed' => 1]);

        $popular = Webcam::find()->orderBy(new Expression('viewed'))->limit(3)->all();
        $arrLangs = ['ru-RU'=>'ru', 'en-US'=>'en'];
         \notgosu\yii2\modules\metaTag\components\MetaTagRegister::register($model, $arrLangs[Yii::$app->language]);

        \Yii::$app->view->registerMetaTag([
            'property' => 'og:image',
            'content' => $model->thumbnail_base_url.'/'.$model->thumbnail_path
        ]);
        \Yii::$app->view->registerMetaTag([
            'property' => 'og:url',
            'content' => $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']
        ]);
        \Yii::$app->view->registerMetaTag([
            'name' => 'title',
            'content' => 'title'
        ]);
        \Yii::$app->view->registerLinkTag(['rel' => 'image_src', 'href' => $model->thumbnail_base_url.'/'.$model->thumbnail_path]);

        $ratings = null;
        $lookup = null;
        if (isset($_COOKIE['rating'])) {
            $json = json_decode($_COOKIE['rating']);

            $lookup = array();
            for ($i = 0; $i < count($json); $i++) {
                $lookup[key($json[$i])] = current($json[$i]);
            }
            if (isset($lookup[$model->id])){
                $ratings = \common\models\Rating::find()->select(['COUNT(*) AS cnt', 'star'])->where(['webcam_id'=>$model->id])->groupBy(['star'])->all();
            }
        }
        

        return $this->render('view', ['model'=>$model, 'popular' => $popular, 'ratings' => $ratings, 
            'lookup'=>$lookup]);
    }

    /**
     * Creates a new Webcam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Webcam();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Updates an existing Webcam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    // /**
    //  * Deletes an existing Webcam model.
    //  * If deletion is successful, the browser will be redirected to the 'index' page.
    //  * @param integer $id
    //  * @return mixed
    //  */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    // /**
    //  * Finds the Webcam model based on its primary key value.
    //  * If the model is not found, a 404 HTTP exception will be thrown.
    //  * @param integer $id
    //  * @return Webcam the loaded model
    //  * @throws NotFoundHttpException if the model cannot be found
    //  */
    // protected function findModel($id)
    // {
    //     if (($model = Webcam::findOne($id)) !== null) {
    //         return $model;
    //     } else {
    //         throw new NotFoundHttpException('The requested page does not exist.');
    //     }
    // }
}
