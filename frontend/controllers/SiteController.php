<?php
namespace frontend\controllers;

use common\models\Page;
use Yii;
use frontend\models\ContactForm;
use yii\web\Controller;
use common\models\Webcam;
use yii\db\Expression;
use common\models\News;
use yii\web\HttpException;
use yii\base\Exception;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            // 'error' => [
            //     'class' => 'yii\web\ErrorAction'
            // ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale'=>[
                'class'=>'common\actions\SetLocaleAction',
                'locales'=>array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {
        $random = Webcam::find()->orderBy(new Expression('rand()'))->limit(1)->one();
        $popular = Webcam::find()
            ->join('LEFT JOIN', '(SELECT webcam_id, AVG(star) avg_rating FROM rating GROUP BY webcam_id) as r', 'r.webcam_id=webcam.id')
            ->orderBy(new Expression('avg_rating DESC'))
            ->limit(3)->all();
        $latest = Webcam::find()->orderBy(new Expression('id DESC'))->limit(6)->all();
        $news = News::find()->limit(3)->all();


        $page = Page::find()->where(['slug' => 'main'])->one();
        if ($page) {
            $arrLangs = ['ru-RU' => 'ru', 'en-US' => 'en'];
            \notgosu\yii2\modules\metaTag\components\MetaTagRegister::register($page, $arrLangs[Yii::$app->language]);
        }

        return $this->render('index', ['random' => $random, 'popular' => $popular, 'latest' => $latest, 'news' => $news]);
    }

    public function actionError()
    {
        $this->layout = 'error';

        if(($exception = Yii::$app->getErrorHandler()->exception) === null)
            $exception = new HttpException(404, Yii::t('yii', 'Page not found.'));

        if($exception instanceof HttpException)
            $code = $exception->statusCode;
        else
            $code = $exception->getCode();

        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = Yii::t('yii', 'Error');
        }

        $message = $exception->getMessage();

        if (Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        }
        else
            return $this->render('error', ['code' => $code, 'name'=>$name, 'exception'=>$exception]);
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->contact(Yii::$app->params['adminEmail'])) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible.'),
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->refresh();
            } else {
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>\Yii::t('frontend', 'There was an error sending email.'),
                    'options'=>['class'=>'alert-danger']
                ]);
            }
        }

        return $this->render('contact', [
            'model' => $model
        ]);
    }
}
