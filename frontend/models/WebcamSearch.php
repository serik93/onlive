<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Webcam;

/**
 * WebcamSearch represents the model behind the search form about `common\models\Webcam`.
 */
class WebcamSearch extends Webcam
{
    public $q;
    public $city;
    public $category;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'author_id', 'updater_id', 'viewed'], 'integer'],
            [['city_id', 'q', 'city', 'category'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Webcam::find();

        $query->joinWith(['city', 'category']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['city'] = [
            'asc' => ['city.name_en' => SORT_ASC],
            'desc' => ['city.name_en' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['category'] = [
            'asc' => ['category.name_en' => SORT_ASC],
            'desc' => ['category.name_en' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['viewed'] = [
            'asc' => ['viewed' => SORT_ASC],
            'desc' => ['viewed' => SORT_DESC],
        ];
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->OrFilterWhere([
            // 'id' => $this->id,
           // 'category_id' => $this->category_id,
            'city_id' => $this->city_id,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
            // 'author_id' => $this->author_id,
            // 'updater_id' => $this->updater_id,
            // 'viewed' => $this->viewed,
        ]);


        $query->orFilterWhere(['like', 'title_en', $this->q])
            ->orFilterWhere(['like', 'title_ru', $this->q])
            ->orFilterWhere(['like', 'address_en', $this->q])
            ->orFilterWhere(['like', 'address_ru', $this->q])
            ->orFilterWhere(['like', 'webcam.desc_ru', $this->q])
            ->orFilterWhere(['like', 'webcam.desc_en', $this->q])
            ->orFilterWhere(['like', 'web_url', $this->q]);

        if (isset($params['category'])) {
            $query->andFilterWhere(['like', 'webcam_category.slug', $params['category']]);
        }
        if (isset($params['city'])) {
            $query->andFilterWhere(['like', 'city.name_en', $params['city']]);
        }

        return $dataProvider;
    }
}
