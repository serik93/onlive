<?php
return [
    //'class'=>'yii\web\UrlManager',
    'enablePrettyUrl'=>true,
    'showScriptName'=>false,
    //'class' => 'frontend\components\LangUrlManager',
    'class' => 'codemix\localeurls\UrlManager',
    'languages' => ['ru'=>'ru-RU', 'en'=>'en-US'],
    'enableLanguageDetection' => false,
    'enableDefaultLanguageUrlCode' => false,
    //'enableLanguagePersistence' => false,
    'rules'=> [
        // Pages
        // '<language:\w+>/<controller>/<action>/<id:\d+>/<title>' => '<controller>/<action>',
        // '<language:\w+>/<controller>/<id:\d+>/<title>' => '<controller>/index',
        // '<language:\w+>/<controller>/<action>/<id:\d+>' => '<controller>/<action>',
        // '<language:\w+>/<controller>/<action>' => '<controller>/<action>',
        // '<language:\w+>/<controller>' => '<controller>',
        ['pattern'=>'cams/<city>-webcams-online-<category>', 'route' => 'webcam/city'],
        ['pattern'=>'cams/<city>-webcams-online', 'route' => 'webcam/city'],
        ['pattern'=>'cams/<city>-webcams-online/<slug>', 'route'=>'webcam/view'],
        ['pattern'=>'cams/webcams-online-<category>', 'route' => 'webcam/category'],
        ['pattern'=>'news', 'route' => 'news/index'],
        ['pattern'=>'favourite', 'route' => 'favourite/index'],
        ['pattern'=>'page/<slug>', 'route'=>'page/view'],

        // Articles
        // ['pattern'=>'article/index', 'route'=>'article/index'],
        // ['pattern'=>'article/attachment-download', 'route'=>'article/attachment-download'],
        // ['pattern'=>'article/<slug>', 'route'=>'article/view'],

        // Webcams
        ['pattern'=>'popular', 'route'=>'webcam/popular'],
        ['pattern'=>'latest', 'route'=>'webcam/latest'],
        ['pattern'=>'cams', 'route'=>'webcam/index'],
        ['pattern'=>'<slug>', 'route'=>'webcam/view'],

        ['pattern'=>'news/<slug>', 'route'=>'news/view'],
        ['pattern'=>'/', 'route'=>'site/index'],
        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/article', 'only' => ['index', 'view', 'options']],
        ['class' => 'yii\rest\UrlRule', 'controller' => 'api/v1/user', 'only' => ['index', 'view', 'options']]
    ]
];
