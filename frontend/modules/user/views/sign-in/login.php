<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\user\models\LoginForm */

$this->title = Yii::t('frontend', 'Login');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(['id' => 'user-login-form']); ?>
  <?php echo $form->field($model, 'identity')->label('')->textInput(['required' => 'required','placeholder'=>Yii::t('frontend', 'login')]) ?>
  <?php echo $form->field($model, 'password')->passwordInput(['required' => 'required', 'placeholder' => Yii::t('backend', 'Password')])->label('') ?>
  <?php echo $form->field($model, 'rememberMe', [
		'template' => "<label class=\"remember\">{input}<span>".Yii::t('frontend', 'Remember Me')."</span></label>\n{error}<label class=\"forgot\">".Html::a(Yii::t('frontend', 'Forgot password'), yii\helpers\Url::to('/user/sign-in/request-password-reset')).'</label>',
  ])->checkbox(['class'=>'form-control'], false) ?>
  <div class="form-group">
		<?php echo Html::submitButton(Yii::t('frontend', 'Login'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
  </div>
  <!-- <div class="form-group"> -->
		<?php echo Html::a(Yii::t('frontend', 'Need an account? Sign up.'), ['signup'], ['class' => 'info-link']) ?>
  <!-- </div> -->
<?php ActiveForm::end(); ?>