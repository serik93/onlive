<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;

/* @var $content string */

$this->beginContent('@frontend/modules//user/views/layouts/base.php')
?>
    <!-- <div class="container"> -->

        <?php if(Yii::$app->session->hasFlash('alert')):?>
            <?php echo \yii\bootstrap\Alert::widget([
                'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
            ])?>
        <?php endif; ?>
		<div class="auth-page">
            <!-- login form -->
            <div id="login-form" class="auth-form">
                <a href="/"><img src="/img/logo.png" title="Главная страница Onlive.kz"></a>
                <?php echo $content ?>
            </div>
	    </div>

    <!-- </div> -->
<?php $this->endContent() ?>