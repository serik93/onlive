$(document).ready(function () {

    "use strict";

    //----------------------------------------------------------------------—
    // DISABLE SCROLLING ON IFRAME
    //----------------------------------------------------------------------—

    $("iframe").attr("scrolling", "no");

    $('.progressbar').each(function () {
        $(this).progressbar({
            value: $(this).data('value'),
            max: $(this).data('max'),
        });
    });

    //------------------------------------------------------------------------
    //	jQuery Bar Rating http://antenna.io/demo/jquery-bar-rating/examples/
    //------------------------------------------------------------------------
    var jsonRatings = getCookie('rating')
    var arr = []
    if (jsonRatings.length)
        arr = JSON.parse(jsonRatings)

    $('.sp-stars-vote').barrating({
        theme: 'fontawesome-stars',
        readonly: true
    });

    $('#vote').on('submit', function (event) {
        var rating = {}
        rating[$('#single-cam-container').data('webcam')] = $('[name=quality]:checked').val()
        arr.push(rating)
        setCookie('rating', JSON.stringify(arr), 300)
        event.preventDefault();
        $.ajax({
            method: 'POST',
            url: $(this).attr('action'),
            data: {
                "Rating[webcam_id]": $('#single-cam-container').data('webcam'),
                "Rating[star]": $('[name=quality]:checked').val()
            },
            success: function (data) {
                if (data == '-1') {
                    alert('Ошибка, попробуйте позже');
                } else {
                    $("#vote").replaceWith(data);
                }
            }
        });
        return false;
    });

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //------------------------------------------------------------------------
    //						HIDDEN RESULTS SETTINGS
    //------------------------------------------------------------------------

    $(".show-comments").click(function () {
        $(this).toggleClass('shown');
        $(".comments-hidden").slideToggle(); //вот так
        if ($(this).hasClass('shown')) {
            $(this).text('Скрыть комментарии');
        } else {
            $(this).text('Показать комментарии');
        }
    });


    //------------------------------------------------------------------------
    //	MENU
    //------------------------------------------------------------------------

    $(document).click(function (e) {
        var t = $(e.target).closest(".header-menu .nav li.has-submenu");
        $(".active").not(t).removeClass("active");
        $(e.target).closest(".header-menu .nav li.has-submenu a").length && t.toggleClass("active");
    });


    //------------------------------------------------------------------------
    // MAGNIFIC POPUP http://dimsemenov.com/plugins/magnific-popup/
    //------------------------------------------------------------------------

    /* INLINE POPUP */
    $('.magnificPopup').magnificPopup({
        removalDelay: 500, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function () {
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        },
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

    /* SINGLE IMAGE */
    $('.magnificImage').magnificPopup({
        type: 'image',
        tLoading: '',
        removalDelay: 500, //delay removal by X to allow out-animation

        callbacks: {
            beforeOpen: function () {
                // just a hack that adds mfp-anim class to markup
                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        },

        closeBtnInside: false,
        closeOnContentClick: true,
        midClick: true
    });

    /* GALLERY MODE */
    var groups = {};
    $('.magnificGallery').each(function () {
        var id = parseInt($(this).attr('data-group'), 30);
        if (!groups[id]) {
            groups[id] = [];
        }
        groups[id].push(this);
    });

    $.each(groups, function () {

        $(this).magnificPopup({
            type: 'image',
            tLoading: '',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            removalDelay: 500, //delay removal by X to allow out-animation

            callbacks: {
                beforeOpen: function () {
                    // just a hack that adds mfp-anim class to markup
                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
            },

            closeBtnInside: false,
            closeOnContentClick: true,
            midClick: true
        })

    });

    // Add it after jquery.magnific-popup.js and before first initialization code
    $.extend(true, $.magnificPopup.defaults, {
        tClose: 'Закрыть (Esc)', // Alt text on close button
        tLoading: 'Загрузка...', // Text that is displayed during loading. Can contain %curr% and %total% keys
        gallery: {
            tPrev: 'Предыдущий', // Alt text on left arrow
            tNext: 'Следующий)', // Alt text on right arrow
            tCounter: '%curr% из %total%' // Markup for "1 of 7" counter
        },
        image: {
            tError: '<a href="%url%">Изоображение</a> не может быть загружено.' // Error message when image could not be loaded
        },
        ajax: {
            tError: '<a href="%url%">Контент</a> пустой.' // Error message when ajax request failed
        }
    });

    $('#paramSearchBtn').click(function () {
        var city = $(this).closest('form').find('#byRegion').val();
        var category = $(this).closest('form').find('#byCategory').val()
        if (city.length > 0 || category.length > 0) {
            window.location.href = '/cams/' + (city.length > 0 ? city + '-' : '') + 'webcams-online' + (category.length > 0 ? '-' + category : '');
        }
    });
});

function addToFavourite(context) {
    if (!$(context).hasClass('liked')) {
        var lang = '';
        if ($('html').attr('lang') == 'en-US')
            lang = '/en'
        $.ajax({
            method: 'POST',
            url: (lang + '/favourite/create'),
            data: "Favourite[webcam_id]=" + context.dataset.webcam,
            success: function (data) {
                if (data == 1)
                    $(context).addClass('liked')

                else if (data == -1) {
                    $.magnificPopup.open({
                        items: {
                            src: '#popup-authform',
                            type: 'inline'
                        },
                        type: 'inline'
                    });
                }
            },
            error: function (data) {
                console.log(data)
            }
        });
    }
    else
        $.ajax({
            method: 'POST',
            url: '/favourite/delete',
            data: {id: context.dataset.id},
            success: function (data) {
                if (data) {
                    $(context).removeClass('liked')
                }
            },
            error: function (data) {
                console.log(data)
            }
        });
    return false;
}