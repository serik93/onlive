<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
//$this->title = Yii::$app->name;

?>  

	<!-- MAIN CONTENT BEGIN -->		
	<div id="main-content">
  
		<!-- random cam container -->
		<div id="random-cam-container">
			 <div class="container">
		
				  <div class="page-heading">
						<h1><img src="/img/webcam.png"><?= Yii::t('frontend', 'Random Webcams') ?></h1>
				  </div>
				  
				  <!-- random cam box --> 
				  <div id="random-cam-box">
						<div class="random-cam-desc clearfix">
							 <div class="random-cam-place">
								  <i class="fa fa-map-marker"></i><?= Yii::$app->language == 'ru-RU' ? $random->city->name_ru.', '.$random->address_ru : $random->city->name_en.', '.$random->address_en ?>
							 </div>  
							 <div class="random-cam-info">
								  <i class="fa fa-info-circle"></i>
								  <?= Html::a(Yii::$app->language == 'ru-RU' ? $random->title_ru : $random->title_en, 
												  Url::to('/cams/'.strtolower($random->city->name_en).'-webcams-online/'.$random->slug)) ?>
							 </div>
						</div>
						<div class="random-cam-frame">
							 <?= $random->url ?>
						</div>
						<div class="random-cam-bottom">
							 <div class="random-cam-left">
								  <ul class="random-cam-stats">
									  <?php $rating = round($random->getRatingAverage());?>
									  <li>
										  <select class="sp-stars-vote">
											  <option value="1" <?=$rating == 1 ? 'selected' : ''?>></option>
											  <option value="2" <?=$rating == 2 ? 'selected' : ''?>></option>
											  <option value="3" <?=$rating == 3 ? 'selected' : ''?>></option>
											  <option value="4" <?=$rating == 4 ? 'selected' : ''?>></option>
											  <option value="5" <?=$rating == 5 ? 'selected' : ''?>></option>
										  </select>
									  </li>
										<li><i class="fa fa-eye"></i><?= $random->viewed; ?></li>
										<li><i class="fa fa-comment"></i> <?= rmrevin\yii\module\Comments\models\Comment::find()->andWhere(['entity' => 'webcam-'.$random->id])->count(); ?></li>
								  </ul>
							 </div>
							 <div class="social">
								 <script type="text/javascript">(function() {
										 if (window.pluso)if (typeof window.pluso.start == "function") return;
										 if (window.ifpluso==undefined) { window.ifpluso = 1;
											 var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
											 s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
											 s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
											 var h=d[g]('body')[0];
											 h.appendChild(s);
										 }})();</script>

								 <div class="pluso" data-background="transparent" data-options="small,square,line,horizontal,counter,theme=04" data-services="vkontakte,facebook,odnoklassniki,twitter,google" data-title="<?= htmlspecialchars($random->title_ru)?>" data-description="Веб камеры Казахстана онлайн. Виртуальные путешествия Onlive.kz" data-url="<?=Url::to('@web/cams/'.strtolower($random->city->name_en).'-webcams-online/'.$random->slug, true)?>"></div>
							 </div>
							 <div class="random-cam-link">
								  <a href="http://<?= $random->web_url ?>"><?= $random->web_url ?></a>
							 </div>                      
						</div>
				  </div>
				  <!--/. random cam box -->       
				  
			 </div>
		</div>
		<!--/. random cam container --> 
			 
			 
		<!-- latest cams list -->   
		<div id="latest-cams" class="cams-block">
			 <div class="container">
				  <div class="section-heading clearfix">
						<h1><?= Yii::t('frontend', 'Latest Added') ?></h1>
						<a href="/latest" class="showall"><?= Yii::t('frontend', 'show all') ?></a>
				  </div>          
				  <ul class="cam-list row clearfix">
						<!-- cam container -->
						<?php foreach ($latest as $key => $value): ?>
							<?php $rating = round($value->getRatingAverage());?>
						<li class="cam-container" data-webcam="<?= $value->id ?>">
							 <div class="cam-desc clearfix">
								  <div class="cam-place">
										<i class="fa fa-map-marker"></i> <?= Yii::$app->language == 'ru-RU' ?  $value->city->name_ru : $value->city->name_en ?>
								  </div>  
								  <div class="cam-info">
										<i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asDateTime($value->updated_at) ?>
								  </div>     
							 </div>
							 <div class="cam-item">
								  <div class="cam-cover">
										<?= Html::a(Html::img($value->thumbnail_base_url.'/'.$value->thumbnail_path), 
												  Url::to('/cams/'.strtolower($value->city->name_en).'-webcams-online/'.$value->slug)) ?>
								  </div>
								  <div class="cam-bottom clearfix">
										<a href="<?= Url::to('/cams/'.strtolower($value->city->name_en).'-webcams-online/'.$value->slug)?>" class="cam-name"><?= Yii::$app->language == 'ru-RU' ?  $value->title_ru : $value->title_en ?></a>
										<div class="cam-bottom-left">
											 <ul class="cam-stats">
												 <li>
													 <select class="sp-stars-vote">
														 <option value="1" <?=$rating == 1 ? 'selected' : ''?>></option>
														 <option value="2" <?=$rating == 2 ? 'selected' : ''?>></option>
														 <option value="3" <?=$rating == 3 ? 'selected' : ''?>></option>
														 <option value="4" <?=$rating == 4 ? 'selected' : ''?>></option>
														 <option value="5" <?=$rating == 5 ? 'selected' : ''?>></option>
													 </select>
												 </li>
												  <li><i class="fa fa-eye"></i><?= $value->viewed ?></li>
												  <li><i class="fa fa-comment"></i><?= rmrevin\yii\module\Comments\models\Comment::find()->andWhere(['entity' => 'webcam-'.$value->id])->count(); ?></li>
											 </ul>   
										</div>
										<?php 
										if ($value->isLiked)
										echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), [''],[
											 'class' => 'addFavotie liked','data-webcam'=>$value->id, 'data-id'=>$value->favourite->id, 'onclick' => "return addToFavourite(this);",]);
										else
										echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), [''],[
											 'class' => 'addFavotie','data-webcam'=>$value->id, 'onclick' => "return addToFavourite(this);",]); ?>
								  </div>
							 </div>
						</li>
						<?php endforeach; ?>
				  </ul>
			 </div>
		</div>
		<!--/. latest cams -->      


		<!-- popular cams list -->  
		<div id="popular-cams" class="cams-block">
			 <div class="container">
				  <div class="section-heading clearfix">
						<h1><?= Yii::t('frontend', 'Popular Cameras') ?></h1>
						<a href="/popular" class="showall"><?= Yii::t('frontend', 'show all') ?></a>
				  </div>
				  <ul class="cam-list row clearfix">           
						<!-- cam container -->
						<?php foreach ($popular as $key => $value): ?>
							<?php $rating = round($value->getRatingAverage());?>
						<li class="cam-container">
							 <div class="cam-desc clearfix">
								  <div class="cam-place">
										<i class="fa fa-map-marker"></i><?= Yii::$app->language == 'ru-RU' ?  $value->city->name_ru : $value->city->name_en ?>
								  </div>  
								  <div class="cam-info">
										<i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asDateTime($value->updated_at) ?>
								  </div>     
							 </div>
							 <div class="cam-item">
								  <div class="cam-cover">
										<?= Html::a(Html::img($value->thumbnail_base_url.'/'.$value->thumbnail_path), 
												  Url::to('/cams/'.strtolower($value->city->name_en).'-webcams-online/'.$value->slug)) ?>
								  </div>
								  <div class="cam-bottom clearfix">
										<a href="<?= Url::to('/cams/'.strtolower($value->city->name_en).'-webcams-online/'.$value->slug)?>" class="cam-name"><?= Yii::$app->language == 'ru-RU' ?  $value->title_ru : $value->title_en ?></a>
										<div class="cam-bottom-left">
											 <ul class="cam-stats">
												 <li>
													 <select class="sp-stars-vote">
														 <option value="1" <?=$rating == 1 ? 'selected' : ''?>></option>
														 <option value="2" <?=$rating == 2 ? 'selected' : ''?>></option>
														 <option value="3" <?=$rating == 3 ? 'selected' : ''?>></option>
														 <option value="4" <?=$rating == 4 ? 'selected' : ''?>></option>
														 <option value="5" <?=$rating == 5 ? 'selected' : ''?>></option>
													 </select>
												 </li>
												  <li><i class="fa fa-eye"></i><?= $value->viewed ?></li>
												  <li><i class="fa fa-comment"></i> <?= rmrevin\yii\module\Comments\models\Comment::find()->andWhere(['entity' => 'webcam-'.$value->id])->count(); ?></li>
											 </ul>   
										</div>
										<?php if ($value->isLiked)
										echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), ['#'],[
											 'class' => 'addFavotie liked','data-webcam'=>$value->id, 'data-id'=>$value->favourite->id, 'onclick' => "return addToFavourite(this);",]);
										else
										echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), ['#'],[
											 'class' => 'addFavotie','data-webcam'=>$value->id, 'onclick' => "return addToFavourite(this);"]);
										 ?>
								  </div>
							 </div>
						</li>
						<?php endforeach; ?>
						<!--/. cam container -->    
				  </ul>
			 </div>
		</div>
		<!--/. popular cams -->
		

  
	</div>
	<!--/. MAIN CONTENT END -->
		  
		      