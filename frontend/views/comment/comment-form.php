<?php
/**
 * comment-form.php
 * @author Revin Roman
 * @link https://rmrevin.ru
 *
 * @var yii\web\View $this
 * @var Comments\forms\CommentCreateForm $CommentCreateForm
 */

use rmrevin\yii\module\Comments;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var Comments\widgets\CommentFormWidget $Widget */
$Widget = $this->context;

?>

    <a name="commentcreateform"></a>
    <?php
    /** @var ActiveForm $form */
    $form = ActiveForm::begin(['id' => 'comments-form']);

    echo Html::activeHiddenInput($CommentCreateForm, 'id');

    if (\Yii::$app->getUser()->getIsGuest()) {
        echo $form->field($CommentCreateForm, 'from')
            ->textInput(['required' => 'required'])->label(Yii::t('frontend', 'Introduce yourself').'<span>*</span>');
    }

    $options = [];

    if ($Widget->Comment->isNewRecord) {
        $options['data-role'] = 'new-comment';
        $options['id'] = 'feedback-msg';
        $options['required'] = 'required';
    }

    echo $form->field($CommentCreateForm, 'text')
        ->textArea($options)->label(Yii::t('frontend', 'Your comment').'<span>*</span>');

    ?>
        <?php
        echo Html::submitButton(\Yii::t('frontend', 'Leave comment'), [
            'class' => 'btn',
        ]);
        ?>
    <?php
    ActiveForm::end();
    ?>