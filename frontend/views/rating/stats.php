<?php
/**
 * @var $ratings array
 */

$total = 0;
foreach($ratings as $rate) {
    $total += $rate['cnt'];
}
if ($total == 0) {
    $total = 1;
}
$data = [];
$names = ['terrible', 'bad', 'normal', 'good', 'excellent'];
for($i = 0; $i < 5; ++$i) {
    $data[$i] = ['percent' => 0, 'count' => 0, 'name' => Yii::t('frontend', $names[$i])];
}
foreach($ratings as $rate) {
    $data[$rate['star'] - 1]['percent'] = round($rate['cnt']/$total*100);
    $data[$rate['star'] - 1]['count'] = $rate['cnt'];
}
?>
<!-- vote -->
<div id="poll">
    <?php foreach($data as $rate):?>
    <div class="poll-stat clearfix">
        <div class="poll-title"><?= htmlspecialchars($rate['name'])?></div>
        <div class="poll-row">
            <div class="poll-percent" style="width: <?=$rate['percent']?>%"></div>
            <div class="poll-row-count"><?=$rate['count']?></div>
        </div>
        <div class="poll-row-percent"><?=$rate['percent']?>%</div>
    </div>
    <?php endforeach;?>
    <div class="poll-text">Проголосовало <strong><?= $total?> человек</strong></div>
</div>
<!--/. vote -->