<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use frontend\models\WebcamSearch;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\HttpException;

/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php')
?>
<div id="error-page">
<div id="error-page-content">
  <img id="error-page-hh-illustration" src="/img/error.png" alt="">
  <p>
	 К сожалению, страница недоступна.
  </p>
  <p>Попробуйте выполнить поиск по другому запросу.</p>
  <div id="yt-masthead">
	 <a id="logo-container" href="http://onlive.kz" title="Главная страница Onlive.kz" class="spf-link">
		<img src="/img/logo.png">
	 </a>
	<div class="search-box">
		<?php
            $form = ActiveForm::begin([
                'id' => 'webcam-search-form',
                'method' => 'get',
                'action' => Url::to('/cams')
            ]);
            echo $form->field(new WebcamSearch(), 'q', ['template'=>'{input}'])->label('')->input('search', ['placeholder'=>Yii::t('frontend', 'What are you searching for?')]);
            echo Html::submitButton('<span>'.Yii::t('frontend','search').'</span>' , ['class' => 'search-button']);
            ActiveForm::end();
         ?>
	</div>
  </div>
</div>
<span id="error-page-vertical-align"></span>
</div>
<?php $this->endContent() ?>