<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\models\WebcamSearch;
/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php')
?>

	<!-- HEADER BEGIN -->	
	<header id="header">
  
		<!-- header ribbon -->
		<div class="header-ribbon">
			 <div class="container clearfix">        
				  <div class="nav clearfix">
						<nav>
							 <ul>
								<li><?= Html::a(Yii::t('frontend','popular'), Url::to('/popular')) ?></li>
								<li><?= Html::a(Yii::t('frontend','latest'), Url::to('/latest')) ?></li>
								<!--<li><?= Html::a(Yii::t('frontend','news'), Url::to('/news/')) ?></li>-->
								<li><a href="<?=Url::to('/page/help')?>"><?= Yii::t('frontend', 'help') ?></a>						  
							 </ul>
						</nav>
				  </div>
				  <div class="right-menu">
						<ul>
							 <!-- <li><a href="#"><i class="fa fa-video-camera"></i> Добавить камеру</a></li> -->
							 <li class="has-submenu"><a href="#"><i class="fa fa-language"></i><?= Yii::$app->params['availableLocales'][Yii::$app->language] ?></a>
								  <ul>
										<li><?= Html::a('Русский',Url::current(['language'=>'ru-RU'])); ?></li>
										<li><?= Html::a('English',Url::current(['language'=>'en-US'])) ?></li>
								  </ul>
							 </li>
						</ul>   
				  </div>
			 </div>              
		</div>  
		<!--/. header ribbon -->    
		
		<!-- header main -->
		<div class="header-main">
			 <div class="container clearfix">
				  <div class="brand">
						<a href="/" title="Главная страница Onlive.kz">Onlive.kz</a>
				  </div>  
				  <div class="search-box">
				  <?php
						$form = ActiveForm::begin([
							 'id' => 'webcam-search-form',
							 'method' => 'get',
							 'action' => Url::to('/cams')
						]);
						echo $form->field(new WebcamSearch(), 'q', ['template'=>'{input}'])->label('')->input('search', ['placeholder'=>Yii::t('frontend', 'What are you searching for?')]);
						echo Html::submitButton('<span>'.Yii::t('frontend','search').'</span>' , ['class' => 'search-button']);
						ActiveForm::end();
					?>
						<!-- <input type="search" placeholder="<? // Yii::t('frontend', 'What are you searching for?') ?>">
						<button type="submit" class="search-button"><span><? //Yii::t('frontend','search') ?></span></button> -->
				  </div>
				  <div class="user-box">
						<ul>
							 <li>
								  <a href="/favourite/">
										<i class="fa fa-heart-o"></i> 
										<span><?= Yii::t('frontend', 'favour') ?></span>
								  </a>                                                            
							 </li>                   
							 <li>    
								  <div class="user-box-auth">
										<i class="fa fa-user"></i>
										<div class="user-links">
											 <?php if (Yii::$app->user->isGuest): ?>
												  <a href="/user/sign-in/login"><?= Yii::t('frontend', 'login') ?></a>
												  <a href="/user/sign-in/signup"><?= Yii::t('frontend', 'registr') ?></a>
											 <?php else: ?>
												  <?= Yii::$app->user->identity->getPublicIdentity() ?>
												  <div><a href="<?= Url::to(['user/sign-in/logout'])?>" data-method="post"><?= Yii::t('frontend', 'Logout') ?></a></div>
											 <?php endif; ?>
										</div>
								  </div>
							 </li>
						</ul>
				  </div>
			 </div>              
		</div>  
		<!--/. header main -->
		
		<!-- header menu -->
		<div class="header-menu">
			 <div class="container clearfix">
				  <div class="nav clearfix">
						<nav>
							 <ul>
								  <li class="has-submenu"><a href="#"><?= Yii::t('frontend', 'byRegion') ?></a>
										<div class="header-submenu">
											 <?php $cities = common\models\City::find()->orderBy('name_ru')->all(); $counter = 0; ?>
											 <?php foreach($cities as $key => $value): ?>
												  <?php if ($counter%6 == 0): ?><ul class="header-submenu-col"><?php endif; ?>
														<li>
															 <?= Html::a(Yii::$app->language == 'ru-RU' ? $value->name_ru.' ('.$value->counter .')': $value->name_en.' ('.$value->counter.')', Url::to('/cams/'.strtolower($value->name_en).'-webcams-online')) ?>
														</li>
												  <?php if ($counter%6 == 5): ?></ul><?php endif; ?>
											 <?php $counter++;endforeach; ?>
										</div>
								  </li>
								  <li class="has-submenu"><a href="#"><?= Yii::t('frontend', 'byCategory') ?></a>
										<div class="header-submenu">
											 <?php $categories = common\models\WebcamCategory::find()->orderBy('name_ru')->all(); $counter = 0; ?>
											 <?php foreach($categories as $key => $value): ?>
												  <?php if ($counter%5 == 0): ?><ul class="header-submenu-col"><?php endif; ?>
														<li>
															 <?= Html::a(Yii::$app->language == 'ru-RU' ? $value->name_ru.' ('.$value->counter.')' : $value->name_en.' ('.$value->counter.')', Url::to('/cams/webcams-online-'.$value->slug)) ?>
														</li>
												  <?php if ($counter%5 == 4): ?></ul><?php endif; ?>
											 <?php $counter++;endforeach; ?>
										</div>
								  </li>                           
								  <li class="has-submenu"><a href="#"><?= Yii::t('frontend', 'byParameters') ?></a>                               
										<div class="header-submenu">    
											 <div id="filters">
											 <?php
											 $form = ActiveForm::begin([
												  'id' => 'webcam-search-params-form',
												  'method' => 'get',
												  'action' => Url::to('/cams')
											 ]);
											 ?>
												  <div class="form-group">
														<select id="byRegion" class="form-control" name="city">
															 <option value="" selected><?= Yii::t('frontend', 'byRegion') ?></option>
															 <?php foreach($cities as $key => $value): ?>
															 <option value="<?= strtolower($value->name_en) ?>"><?= Yii::$app->language == 'ru-RU' ? $value->name_ru : $value->name_en; ?></option>
															 <?php endforeach; ?>
														</select>
												  </div>
												  <div class="form-group">
														<select id="byCategory" class="form-control" name="category">
															 <option value="" selected><?= Yii::t('frontend', 'byCategory') ?></option>
															 <?php foreach($categories as $key => $value): ?>
																  <option value="<?= strtolower($value->slug) ?>"><?= Yii::$app->language == 'ru-RU' ? $value->name_ru : $value->name_en; ?></option>
															 <?php endforeach; ?>
														</select>
												  </div>
												  <div class="form-group">
														<label>
															 <input type="radio" name="sort" value="-viewed"><?=Yii::t('frontend', 'popular')?>
														</label>
														<label>
															 <input type="radio" name="sort" value="-id"><?= Yii::t('frontend', 'latest') ?>
														</label>
												  </div>
												  <div class="form-group">
														<button type="submit" class="btn"><?= Yii::t('frontend','display') ?></button>
												  </div>
												  <?php ActiveForm::end(); ?>
											 </div>
										</div>                          
								  </li>
								  <li><a href="/cams"><?= Yii::t('frontend', 'allCams') ?></a></li>                                        
							 </ul>
						</nav>
				  </div>
			 </div>              
		</div>  
		<!--/. header menu -->      
		

	</header>
	<!--/. HEADER END -->
    
   <?php echo $content ?>
    
	
<?php $this->endContent() ?>



	<!-- FOOTER BEGIN -->                   
	<footer id="footer">
		 <div class="container">
			  <div class="footer-left">
					<span class="copyright">© Onlive.kz</span>                           
					<ul class="footer-menu">
						 <!--<li><a href="<?=Url::to('/page/ads')?>"><?= Yii::t('frontend', 'ads') ?></a>-->
						 <li><a href="<?=Url::to('/page/holders')?>"><?= Yii::t('frontend', 'right holders') ?></a>
						 <li><a href="<?=Url::to('/page/help')?>"><?= Yii::t('frontend', 'help') ?></a>
						 <!--<li><a href="#map"><?= Yii::t('frontend', 'sitemap') ?></a>-->
					</ul>
			  </div>
			  <div class="footer-right">
					<span><?= Yii::t('frontend', 'We are in socials') ?></span>
					<ul class="footer-social">
						 <li><a href="#"><i class="fa fa-vk"></i></a></li>
						 <li><a href="#"><i class="fa fa-facebook"></i></a></li>
						 <li><a href="#"><i class="fa fa-google-plus"></i></a></li>          
					</ul>       
			  </div>
		 </div>
	</footer>
	<!--/. FOOTER END -->


	<!-- POPUP AUTHFORM BEGIN -->	
	<div class="popup-authform mfp-hide mfp-with-anim" id="popup-authform">
		<div class="popup-title"><?= Yii::t('frontend', 'Liked?') ?></div>
		<div class="popup-subtitle">
			<?= Yii::t('frontend', 'PLEASE LOGIN') ?>
		</div>
	</div>
	<!--/. POPUP AUTHFORM END -->	