<?php
use yii\helpers\Html;
/* @var $this \yii\web\View */
/* @var $content string */

\frontend\assets\FrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?= Yii::$app->language ?>"> <!--<![endif]-->
<html lang="<?= Yii::$app->language ?>" class='no-js'>
<!-- html manifest="app.cache" -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta charset="<?php echo Yii::$app->charset ?>"/>
	<meta name="author" content="www.kulseitov.kz"/>
	<meta name="copyright" content=""/>
	<title><?php echo Html::encode($this->title) ?></title>
	<?php $this->head() ?>
	<link rel="icon" type="image/png" href="/img/favicon.png">
	<?php echo Html::csrfMetaTags() ?>
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-85444279-1', 'auto');
	  ga('send', 'pageview');

	</script>
	
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		 (function (d, w, c) {
			  (w[c] = w[c] || []).push(function() {
					try {
						 w.yaCounter40121650 = new Ya.Metrika({
							  id:40121650,
							  clickmap:true,
							  trackLinks:true,
							  accurateTrackBounce:true,
							  webvisor:true,
							  trackHash:true
						 });
					} catch(e) { }
			  });

			  var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
			  s.type = "text/javascript";
			  s.async = true;
			  s.src = "https://mc.yandex.ru/metrika/watch.js";

			  if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
			  } else { f(); }
		 })(document, window, "yandex_metrika_callbacks");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/40121650" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	
</head>
<body>
<?php $this->beginBody() ?>
<div id="site-wrapper">
    <?php echo $content ?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
