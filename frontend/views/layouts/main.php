<?php
/* @var $this \yii\web\View */
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;

/* @var $content string */

$this->beginContent('@frontend/views/layouts/base.php')
?>
		  <!-- breadcrumbs begin -->		
        <?php if (isset($this->params['breadcrumbs']) && !empty($this->params['breadcrumbs'])): ?>
        <div class="breadcrumbs-container">
            <div class="container">
                <?php echo Breadcrumbs::widget([
                    'links' => $this->params['breadcrumbs'],
                    'options' => ['class' => 'breadcrumbs clearfix']
                ]) ?>
            </div>
        </div>
        <?php endif; ?>
		  <!--/. breadcrumbs end -->	
			
            <?php if(Yii::$app->session->hasFlash('alert')):?>
                <?php echo \yii\bootstrap\Alert::widget([
                    'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                    'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
                ])?>
            <?php endif; ?>

            <!-- Example of your ads placing -->
            <?php echo \common\widgets\DbText::widget([
                'key' => 'ads-example'
            ]) ?>

            <?php echo $content ?>

<?php $this->endContent() ?>