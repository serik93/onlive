<?php
use yii\widgets\ListView;
/* @var $this yii\web\View */

	$this->params['breadcrumbs'][] = ['label'=>Yii::t('frontend','allCams'), 'url'=>['/cams/']];
	$this->params['breadcrumbs'][] = $category->name;
	$this->title = $category->name;

?>

<!-- MAIN CONTENT BEGIN -->	
<div id="main-content">

	<!-- cams list -->  
	<div id="popular-cams" class="cams-block">
		 <div class="container">
		  	<div class="section-heading clearfix">
				<div class="text-results"><?= Yii::t('frontend','FOUND_RESULTS', ['total'=>$dataProvider->totalCount]) ?></div>
				<h1><?= $category->name ?></h1>
		  	</div>
			<!-- cam container -->
			<?php $widget = \yii\widgets\ListView::begin([
			  'dataProvider'=>$dataProvider,
			  'itemView'=>'_item',
			  'itemOptions' => ['tag'=>'li', 'class'=>'cam-container'],
			  'summary' => '',
			  'options' => [
			  	'tag'=>'ul', 
			  	'class'=>'cam-list row clearfix'
			  ]
			]);
			?>
			<ul class="cam-list row clearfix">
				<?php echo $widget->renderItems();  ?>
			</ul>
			<?php echo $widget->renderPager(); ?>
			<!--/. cam container -->
		 </div>

	</div>
	<!--/. cams list  -->
	
</div>
<!--/. MAIN CONTENT END -->	