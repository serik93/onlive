<?php

use yii\helpers\Html;
use rmrevin\yii\module\Comments;
use yii\widgets\DetailView;
use common\widgets\Banner;

/* @var $this yii\web\View */
/* @var $model common\models\Webcam */

$this->title = Yii::$app->language == 'ru-RU' ? $model->title_ru : $model->title_en;
if (isset(Yii::$app->request->queryParams['city']))
    $this->params['breadcrumbs'][] = ['label'=>Yii::t('frontend','Web Cameras online in {city} - journey along Kazakhstan', ['city'=>$model->city->name]), 'url'=>'/cams/'.strtolower($model->city->name_en).'-webcams-online'];
$this->params['breadcrumbs'][] = $this->title;

$rating = round($model->getRatingAverage());

?>
<!-- MAIN CONTENT BEGIN -->		
<div id="main-content">

	<!-- single cam container -->		
	<div id="single-cam-container" data-webcam="<?= $model->id ?>">
	  <div class="container">

			<div class="page-heading"><h1><img src="/img/webcam.png"><?= Html::encode($this->title) ?></h1></div>
			<div class="scam-left">
				 <div id="single-cam-box">
					  <div class="single-cam-frame">
							<?= $model->url ?>
					  </div>
					  <div class="single-cam-bottom">
							<div class="single-cam-left">
								 <ul class="single-cam-stats">
									  <li>
										  <select class="sp-stars-vote">
											  <option value="1" <?=$rating == 1 ? 'selected' : ''?>></option>
											  <option value="2" <?=$rating == 2 ? 'selected' : ''?>></option>
											  <option value="3" <?=$rating == 3 ? 'selected' : ''?>></option>
											  <option value="4" <?=$rating == 4 ? 'selected' : ''?>></option>
											  <option value="5" <?=$rating == 5 ? 'selected' : ''?>></option>
										  </select>
									  </li>
									  <li><i class="fa fa-eye"></i><?= $model->viewed ?></li>
									  <li><i class="fa fa-comment"></i><?= rmrevin\yii\module\Comments\models\Comment::find()->andWhere(['entity' => 'webcam-'.$model->id])->count(); ?></li>
								 </ul>
							</div>
							<div class='social'>
							<script type="text/javascript">(function() {
							  if (window.pluso)if (typeof window.pluso.start == "function") return;
							  if (window.ifpluso==undefined) { window.ifpluso = 1;
							    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
							    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
							    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
							    var h=d[g]('body')[0];
							    h.appendChild(s);
							  }})();</script>
							  
							<div class="pluso" data-background="transparent" data-options="small,square,line,horizontal,counter,theme=04" data-services="vkontakte,facebook,odnoklassniki,twitter,google" data-title="Onlive.kz" data-description="Веб камеры Казахстана онлайн. Виртуальные путешествия Onlive.kz"></div>
							</div>
							<?php if (isset($model->favourite))
									  echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), ['#'],[
											'class' => 'addFavotie liked','data-webcam'=>$model->id, 'data-id'=>$model->favourite->id, 'onclick' => "addToFavourite(this);return false;",]);
									  else
									  echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), ['#'],[
											'class' => 'addFavotie','data-webcam'=>$model->id, 'onclick' => "addToFavourite(this);return false;",]); ?>                 
							<div class="single-cam-link">
								 <a href="http://<?=$model->web_url ?>"><?= $model->web_url ?></a>
							</div>
					  </div>
				 </div>
				 <!-- add -->    
				 <div class="add">
					 <?= Banner::widget(['slot' => 'long'])?>
				 </div>
				 <!--/. add -->                
			</div>
			<!-- scam right -->                     
			<div class="scam-right">
			
				 <!-- vote -->
				 <?php if (!isset($lookup[$model->id])): ?>
				 <form id="vote" action="<?= Yii::$app->language == 'ru-RU' ? '/rating/create' : '/en/rating/create'?>">
					  <div class="form-group">
							<input type="radio" id="rating_0" name='quality' value="1">
							<label for="rating_0"><?= Yii::t('frontend', 'terrible') ?></label>
					  </div>
					  <div class="form-group">
							<input type="radio" id="rating_1" name="quality" value="2">
							<label for="rating_1"><?= Yii::t('frontend', 'bad') ?></label>
					  </div>
					  <div class="form-group">
							<input type="radio" id="rating_2" name="quality" value="3">
							<label for="rating_2"><?= Yii::t('frontend', 'normal') ?></label>
					  </div>
					  <div class="form-group">
							<input type="radio" id="rating_3" name="quality" value="4">
							<label for="rating_3"><?= Yii::t('frontend', 'good') ?></label>
					  </div>
					  <div class="form-group">
							<input type="radio" id="rating_4" name="quality" value="5">
							<label for="rating_4"><?= Yii::t('frontend', 'excellent') ?></label>
					  </div>  
					  <div class="form-group">
							<button type="submit" class="btn"><?= Yii::t('frontend', 'Estimate quality') ?></button>
					  </div>                          
				 </form>
				 <?php else: ?>
					 <?= $this->render('//rating/stats', ['ratings' => $model->getRatings()])?>
				 <?php endif; ?>
				 <!--/. vote --> 

				 <!-- add -->    
				 <div class="add">
					 <?= Banner::widget(['slot' => 'square'])?>
				 </div>
				 <!--/. add -->  
											
			</div>
	  </div>
	</div>
	<!--/. single cam container -->	 
	 
	<!-- single cam content --> 
	<div id="single-cam-content">
		 <div class="container">
		 
			  <div class="single-cam-description">
					<p>
						 <?= Yii::$app->language == 'ru-RU' ? $model->desc_ru : $model->desc_en ?> 
					</p>
			  </div>
			  
			  <div class="comments-container">
					<div class="show-comments"><?= Yii::t('frontend', 'show comment') ?></div>                        
					<div class="comments-hidden">
						 <?php 
							  echo Comments\widgets\CommentListWidget::widget([
									'entity' => (string) 'webcam-'.$model->id, // type and id
									'theme' => 'comment'
							  ]);
						 ?>
					</div>
			  </div>  
			  
		 </div>
	</div>
	<!--/. single cam content -->   

	<!-- popular cams list -->  
	<div id="popular-cams" class="cams-block">
		 <div class="container">
			  <div class="section-heading clearfix">
					<h1><?= Yii::t('frontend', 'Popular Cameras') ?></h1>
					<a href="/popular" class="showall"><?= Yii::t('frontend', 'show all') ?></a>
			  </div>
			  <ul class="cam-list row clearfix">           
					<!-- cam container -->
					<?php foreach ($popular as $key => $value): ?>
					<li class="cam-container">
						 <div class="cam-desc clearfix">
							  <div class="cam-place">
									<i class="fa fa-map-marker"></i> <?= Yii::$app->language == 'ru-RU' ? $value->city->name_ru : $value->city->name_en ?>
							  </div>  
							  <div class="cam-info">
									<i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asDateTime($value->updated_at) ?>
							  </div>     
						 </div>
						 <div class="cam-item">
							  <div class="cam-cover">
									<a href="<?= $value->slug ?>">
										 <img src="<?= $value->thumbnail_base_url.'/'.$value->thumbnail_path ?>">
									</a>
							  </div>
							  <div class="cam-bottom clearfix">
									<a href="<?= $value->slug ?>" class="cam-name"><?= Yii::$app->language == 'ru-RU' ?  $value->title_ru : $value->title_en ?></a>
									<div class="cam-bottom-left">
										 <ul class="cam-stats">
											  <li><i class="fa fa-eye"></i><?= $value->viewed ?></li>
											  <li><i class="fa fa-comment"></i><?= rmrevin\yii\module\Comments\models\Comment::find()->andWhere(['entity' => 'webcam-'.$value->id])->count(); ?></li>
										 </ul>   
									</div>
									<?php if (isset($value->favourite))
													echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), ['#'],[
														 'class' => 'addFavotie liked','data-webcam'=>$value->id, 'data-id'=>$value->favourite->id, 'onclick' => "addToFavourite(this);return false;",]);
													else
													echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), ['#'],[
														 'class' => 'addFavotie','data-webcam'=>$value->id, 'onclick' => "addToFavourite(this);return false;",]); ?>
							  </div>
						 </div>
					</li>
					<?php endforeach; ?>
					<!--/. cam container -->    
			  </ul>
		 </div>
	</div>
	<!--/. popular cams -->     
	
</div>
<!--/. MAIN CONTENT END -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
