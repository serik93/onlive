<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Webcam */

$this->title = Yii::t('frontend', 'Update {modelClass}: ', [
    'modelClass' => 'Webcam',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Webcams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('frontend', 'Update');
?>
<div class="webcam-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
