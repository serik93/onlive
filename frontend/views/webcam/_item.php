<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;

if (isset($_COOKIE['rating'])) {

    $json = json_decode($_COOKIE['rating']);

    $lookup = array();
    for ($i = 0; $i < count($json); $i++) {
        $lookup[key($json[$i])] = current($json[$i]);
    }
    
}
$rating = round($model->getRatingAverage());
?>
<!-- <li class="cam-container"> -->
    <div class="cam-desc clearfix">
        <div class="cam-place">
            <i class="fa fa-map-marker"></i> 
            <?= Yii::$app->language == 'ru-RU' ? $model->city->name_ru : $model->city->name_en ?>
        </div>  
        <div class="cam-info">
            <i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asDateTime($model->updated_at) ?>
        </div>     
    </div>
    <div class="cam-item">
        <div class="cam-cover">
            <a href="<?= isset($model->city) ? '/cams/'.strtolower($model->city->name_en).'-webcams-online/':'' ?><?= $model->slug ?>">
                <img src="<?= $model->thumbnail_base_url.'/'.$model->thumbnail_path ?>">
            </a>                                    
        </div>
        <div class="cam-bottom clearfix">
            <a href="<?= isset($model->city) ? '/cams/'.strtolower($model->city->name_en).'-webcams-online/':'' ?><?= $model->slug ?>" class="cam-name"><?= Yii::$app->language == 'ru-RU' ?  $model->title_ru : $model->title_en ?></a>
            <div class="cam-bottom-left">
                <ul class="cam-stats">
                    <li>
                        <select class="sp-stars-vote">
                            <option value="1" <?=$rating == 1 ? 'selected' : ''?>></option>
                            <option value="2" <?=$rating == 2 ? 'selected' : ''?>></option>
                            <option value="3" <?=$rating == 3 ? 'selected' : ''?>></option>
                            <option value="4" <?=$rating == 4 ? 'selected' : ''?>></option>
                            <option value="5" <?=$rating == 5 ? 'selected' : ''?>></option>
                        </select>
                    </li>
                    <li><i class="fa fa-eye"></i><?= $model->viewed ?></li>
                    <li><i class="fa fa-comment"></i><?= rmrevin\yii\module\Comments\models\Comment::find()->andWhere(['entity' => 'webcam-'.$model->id])->count(); ?></li>
                </ul>   
            </div>
            <?php if ($model->isLiked)
                    echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), [''],[
                         'class' => 'addFavotie liked','data-webcam'=>$model->id, 'data-id'=>$model->favourite->id, 'onclick' => "return addToFavourite(this);",]);
                    else
                    echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), [''],[
                         'class' => 'addFavotie','data-webcam'=>$model->id, 'onclick' => "return addToFavourite(this);",]); ?>
        </div>
    </div>
<!-- </li> -->