<?php
/* @var $this yii\web\View */
$this->title = Yii::t('frontend', 'Popular Cameras')
?>

<!-- MAIN CONTENT BEGIN -->	
<div id="main-content">

	<!-- popular cams list -->  
	<div id="popular-cams" class="cams-block">
		 <div class="container">
			  <div class="section-heading clearfix">
					<h1><?= Yii::t('frontend', 'Popular Cameras') ?></h1>
			  </div>
			  <!-- cam container -->
				<?php $widget = \yii\widgets\ListView::begin([
				  'dataProvider'=>$dataProvider,
				  'itemView'=>'_item',
				  'itemOptions' => ['tag'=>'li', 'class'=>'cam-container'],
				  'summary' => '',
				  'options' => [
				  	'tag'=>'ul', 
				  	'class'=>'cam-list row clearfix'
				  ]
				]);
				?>
				<ul class="cam-list row clearfix">
					<?php echo $widget->renderItems();  ?>
				</ul>
				<?php echo $widget->renderPager(); ?>
				<!--/. cam container -->
		 </div>
	</div>
	<!--/. popular cams list --> 
	
</div>
<!--/. MAIN CONTENT END -->	