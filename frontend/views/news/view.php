<?php

use common\widgets\Banner;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'news'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- MAIN CONTENT BEGIN -->		
<div id="main-content">

	<!-- news single -->                    
	<div id="news-single">
		 <div class="container">
			  <h1><?= Yii::$app->language == 'ru-RU' ? $model->title_ru : $model->title ?></h1>
			  <div class="news-single-container clearfix">
					<div class="news-photo">
						 <img src="<?= $model->thumbnail_base_url.'/'.$model->thumbnail_path ?>">
					</div>
					<?= Yii::$app->language == 'ru-RU' ? $model->desc_ru : $model->desc_en ?>
			  </div>
			  
			  <!-- add -->    
			  <div class="add">
				  <?= Banner::widget(['slot' => 'news'])?>
			  </div>
			  <!--/. add -->                          
			  
		 </div>
	</div>
	<!--/. news single -->

	<!-- latest news -->                    
	<div class="latest-news">
		 <div class="container">
			  <h1><?= Yii::t('frontend', 'Our announcements and news') ?></h1>
			  <ul class="news-list clearfix">                  
					<!-- news container -->
					<?php echo \yii\widgets\ListView::widget([
						 'dataProvider'=>$latest,
						 'pager'=>[
							  'hideOnSinglePage'=>true,
						 ],
						 'itemView'=>'_item',
						 'summary' => '',
					])?>
					<!--/. news container -->
			  </ul>                               
		 </div>
	</div>
	<!--/. latest news -->   

</div>
<!--/. MAIN CONTENT END -->		