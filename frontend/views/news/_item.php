<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;

?>

<li class="news-container clearfix">
	<div class="news-photo">
		<a href="/news/<?=$model->slug ?>"><img src="<?= $model->thumbnail_base_url.'/'.$model->thumbnail_path ?>"></a>
	</div>
	<div class="news-desc">
		<div class="news-date"><?= Yii::$app->formatter->asDateTime($model->updated_at) ?></div>
		<a href="/news/<?=$model->slug ?>" class="news-title"><?= Yii::$app->language == 'ru-RU' ? $model->title_ru : $model->title ?></a>
		<?= \yii\helpers\StringHelper::truncate(Yii::$app->language == 'ru-RU' ? $model->desc_ru : $model->desc_en, 360, '...', null, true); ?>
	</div>
</li>