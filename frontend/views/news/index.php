<?php
/* @var $this yii\web\View */
$this->title = Yii::t('frontend', 'Our announcements and news');

$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Our announcements and news'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>

<!-- MAIN CONTENT BEGIN -->		
<div id="main-content">

	<!-- latest news -->
	<div class="latest-news">
		 <div class="container">
			  <div class="section-heading clearfix">
					<h1><?= Yii::t('frontend', 'Our announcements and news') ?></h1>
			  </div>
			  <ul class="news-list clearfix"> 
				  <!-- news container -->
				  <?php echo \yii\widgets\ListView::widget([
						 'dataProvider'=>$dataProvider,
						 'pager'=>[
							  'hideOnSinglePage'=>true,
						 ],
						 'itemView'=>'_item',
						 'summary' => '',
					])?>
					<!--/. news container -->                       
			  </ul>                               
		 </div>
	</div>
	<!--/. latest news -->
	
</div>
<!--/. MAIN CONTENT END -->	