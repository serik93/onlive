<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = Yii::$app->language == 'ru-RU' ? $model->title_ru : $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- MAIN CONTENT BEGIN -->
<div id="main-content">

    <!-- simple page -->
    <div id="simple-page">
        <div class="container">
            <h1><?= htmlspecialchars($this->title)?></h1>
            <div class="simple-page-container clearfix">
                <?= Yii::$app->language == 'ru-RU' ? $model->body_ru : $model->body ?>
            </div>

        </div>
    </div>
    <!--/. simple page  -->

</div>
<!--/. MAIN CONTENT END -->		