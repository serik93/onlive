<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;
    
?>

<li class="cam-container">
    <div class="cam-desc clearfix">
        <div class="cam-place">
            <i class="fa fa-map-marker"></i> 
            <?= (Yii::$app->language == 'ru-RU' ? $model->webcam->city->name_ru : $model->webcam->city->name_en ) ?>
        </div>  
        <div class="cam-info">
            <i class="fa fa-clock-o"></i> <?= Yii::$app->formatter->asDateTime($model->webcam->updated_at) ?>
        </div>     
    </div>
    <div class="cam-item">
        <div class="cam-cover">
            <a href="<?= isset($model->webcam->city) ? '/cams/'.strtolower($model->webcam->city->name_en).'-webcams-online/':'' ?><?= $model->webcam->slug ?>">
                <img src="/img/photo.jpg">
            </a>                                    
        </div>
        <div class="cam-bottom clearfix">
            <a href="<?= isset($model->webcam->city) ? '/cams/'.strtolower($model->webcam->city->name_en).'-webcams-online/':'' ?><?= $model->webcam->slug ?>" class="cam-name"><?= Yii::$app->language == 'ru-RU' ?  $model->webcam->title_ru : $model->webcam->title_en ?></a>
            <div class="cam-bottom-left">
                <div class="sp-stars">
                    <select class="sp-stars-vote">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>
                </div>
                <ul class="cam-stats">
                    <li><i class="fa fa-eye"></i><?= $model->webcam->viewed ?></li>
                    <li><i class="fa fa-comment"></i><?= rmrevin\yii\module\Comments\models\Comment::find()->andWhere(['entity' => 'webcam-'.$model->webcam->id])->count(); ?></li>
                </ul>   
            </div>
            <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), ['#'],[
                                        'class' => 'addFavotie liked','data-webcam'=>$model->webcam->id, 'data-id'=>$model->id, 'onclick' => "addToFavourite(this);return false;",]);
                                    // else
                                    // echo Html::a(Html::tag('i', '', ['class' => 'fa fa-heart']), ['#'],[
                                    //     'class' => 'addFavotie','data-webcam'=>$model->id, 'onclick' => "addToFavourite(this);return false;",]); 
                                        ?>
        </div>
    </div>
</li>