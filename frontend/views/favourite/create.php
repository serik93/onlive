<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Favourite */

$this->title = Yii::t('common', 'Create {modelClass}', [
    'modelClass' => 'Favourite',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Favourites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="favourite-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
