<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Favourite */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
    'modelClass' => 'Favourite',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Favourites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="favourite-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
