<?php
/* @var $this yii\web\View */
$this->title = Yii::t('frontend', 'Favourites');

$this->params['breadcrumbs'][] = Yii::t('frontend', 'Favourites');
?>
<div class="main-content">
<!-- popular cams list -->  
<div id="popular-cams" class="cams-block">
    <div class="container">
        <div class="section-heading clearfix">
            <div class="text-results"><?= Yii::t('frontend','FOUND_RESULTS', ['total'=>$dataProvider->totalCount]) ?></div>
            <h1><?= Yii::t('frontend', 'Favourites') ?></h1>
        </div>
        <ul class="cam-list row clearfix">
            <!-- cam container -->
            <?php echo \yii\widgets\ListView::widget([
                'dataProvider'=>$dataProvider,
                'pager'=>[
                    'hideOnSinglePage'=>true,
                ],
                'itemView'=>'_item',
                'summary' => '',
            ])?>
        </ul>
    </div>
</div>
<!--/. popular cams -->     
</div>