<?php
namespace common\widgets;
use common\models\Banner as BannerModel;
use yii\db\Expression;

/**
 * Shows random banner
 */
class Banner extends \yii\bootstrap\Widget
{

    public $slot;

    public function run() {
        $banner = BannerModel::find()->where(['status' => BannerModel::STATUS_PUBLISHED, 'slot' => $this->slot])
            ->orderBy(new Expression('RAND()'))
            ->limit(1)
            ->one();
        if ($banner) {
            return $banner->body;
        }
        return '';
    }
}
