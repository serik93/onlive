<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 *
 * @property Webcam[] $webcams
 */
class City extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
                'languages' => ['en', 'ru'],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en'], 'required'],
            [['desc', 'desc_ru'], 'safe'],
            [['name_ru', 'name_en'], 'string', 'max' => 255],
            ['name_en', 'filter', 'filter'=>'strtolower'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'name_ru' => 'Название (ru)',
            'name_en' => 'Название (en)',
            'desc' => 'Описание (en)',
            'desc_ru' => 'Описание (ru)'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebcams()
    {
        return $this->hasMany(Webcam::className(), ['city_id' => 'id']);
    }

    public function getDescription()
    {
        return Yii::$app->language == 'ru-RU' ? $this->desc_ru : $this->desc;
    }

    public function getName()
    {
        return Yii::$app->language == 'ru-RU' ?  $this->name_ru : $this->name_en;
    }

}
