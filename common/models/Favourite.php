<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "favourite".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $webcam_id
 * @property integer $created_at
 *
 * @property User $user
 * @property Webcam $webcam
 */
class Favourite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'favourite';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => 'updater_id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['webcam_id'], 'required'],
            [['created_at', 'updater_id', 'user_id'],'safe'],
           //[['webcam_id', 'user_id'], 'unique', 'attributes' => ['webcam_id', 'user_id']],
            [['user_id', 'webcam_id', 'created_at'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['webcam_id'], 'exist', 'skipOnError' => true, 'targetClass' => Webcam::className(), 'targetAttribute' => ['webcam_id' => 'id']],
            [['updater_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updater_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'webcam_id' => Yii::t('frontend', 'Webcam ID'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updater_id' => 'Обновлено Автором'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebcam()
    {
        return $this->hasOne(Webcam::className(), ['id' => 'webcam_id']);
    }

    /** 
    * @return \yii\db\ActiveQuery
    */ 
    public function getUpdater()
    {
       return $this->hasOne(User::className(), ['id' => 'updater_id']); 
    }
}