<?php

namespace common\models;

use common\models\Rating;
use Yii;

use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

use trntv\filekit\behaviors\UploadBehavior;

use yii\db\Expression;

use v0lume\yii2\metaTags\MetaTagBehavior;

use \common\models\City;
use \common\models\WebcamCategory;
use \common\models\Favourite;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "webcam".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $slug
 * @property integer $title_en
 * @property integer $title_ru
 * @property string $desc_ru
 * @property string $desc_en
 * @property integer $city_id
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property string $created_at
 * @property string $updated_at
 * @property integer $author_id
 * @property integer $updater_id
 * @property integer $viewed
 * @property string $url
 * @property string $address_en
 * @property string $address_ru
 * @property string $web_url
 *
 * @property City $city
 * @property User $author
 * @property WebcamCategory $category
 */
class Webcam extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $thumbnail;

    protected $_oldAttributes;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
                
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'updater_id',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_VALIDATE => ['author_id', 'updater_id']
                ]
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title_en',
                'immutable' => true
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ],
            // 'MetaTag' => [
            //     'class' => MetaTagBehavior::className(),
            // ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
                'languages' => ['en', 'ru'],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'webcam';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_en', 'title_ru', 'desc_ru', 'desc_en', 'city_id'], 'required'],
            [['category_id', 'city_id', 'author_id', 'updater_id', 'viewed'], 'integer'],
            [['desc_ru', 'desc_en', 'url'], 'string'],
            [['created_at', 'updated_at', 'address_en', 'address_ru', 'thumbnail', 'thumbnail_path', 'web_url'], 'safe'],
            [['slug', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => WebcamCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            //[['updated_at'], 'date','format' => 'd-M-yyyy'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'slug' => 'ЧПУ',
            'title_en' => 'Заголовок на английском',
            'title_ru' => 'Заголовок на русском',
            'address_en' => 'Адрес (en)',
            'address_ru' => 'Адрес (ru)',
            'desc_ru' => 'Описание на русском',
            'desc_en' => 'Описание на английском',
            'city_id' => 'Город',
            'thumbnail' => 'Обложка',
            'thumbnail_base_url' => 'Thumbnail Base Url',
            'thumbnail_path' => 'Thumbnail Path',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'author_id' => 'Автор',
            'updater_id' => Yii::t('common', 'Updater'),
            'url' => 'URL видео',
            'web_url' => 'Website'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(WebcamCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavourite()
    {
        return $this->hasOne(Favourite::className(), ['webcam_id' => 'id']);
    }

    public function getIsLiked()
    {
        return (isset($this->favourite) &&  !Yii::$app->user->isGuest && $this->favourite->user->id == Yii::$app->user->id);
    }

    public function getRatings()
    {
        return Rating::find()
            ->where(['webcam_id' => $this->id])
            ->select(['COUNT(*) AS cnt', 'star'])
            ->groupBy(['star'])
            ->asArray()
            ->all();
    }

    public function getRatingAverage()
    {
        $list = $this->getRatings();
        $total = 0;
        $result = 0;
        foreach($list as $rate) {
            $result = $rate['star'] * $rate['cnt'];
            $total += $rate['cnt'];
        }
        return $result / ($total ? $total: 1);
    }

    /**
     * @inheritdoc
     * @return WebcamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WebcamQuery(get_called_class());
    }

    public function afterFind()
    {
        $this->_oldAttributes = $this->attributes;
        return parent::afterFind();
    }

    public function beforeSave()
    {
        if ($this->isNewRecord)
        {
            $city = City::findOne($this->city_id);
            $city->updateCounters(['counter' => 1]);

            $city = WebcamCategory::findOne($this->category_id);
            $city->updateCounters(['counter' => 1]);
        }

        elseif(isset($this->_oldAttributes['city_id']) && $this->city_id != $this->_oldAttributes['city_id'])
        {
            $city = City::findOne($this->_oldAttributes['city_id']);
            $city->updateCounters(['counter' => -1]);

            $city = City::findOne($this->city_id);
            $city->updateCounters(['counter' => 1]);
        }
        elseif(isset($this->_oldAttributes['category_id']) && $this->category_id != $this->_oldAttributes['category_id'])
        {
            $category = WebcamCategory::findOne($this->_oldAttributes['category_id']);
            $category->updateCounters(['counter' => -1]);

            $category = WebcamCategory::findOne($this->category_id);
            $category->updateCounters(['counter' => 1]);
        }

        return parent::beforeSave();
    }

}
