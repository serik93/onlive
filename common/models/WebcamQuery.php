<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Webcam]].
 *
 * @see Webcam
 */
class WebcamQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Webcam[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Webcam|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
