<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "city_content".
 *
 * @property integer $id
 * @property integer $lang_id
 * @property integer $parent_id
 * @property string $name
 *
 * @property City $parent
 */
class CityContentRu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang_id', 'parent_id', 'name'], 'required'],
            [['lang_id', 'parent_id'], 'integer'],
            [['name'], 'string', 'max' => 11],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lang_id' => Yii::t('app', 'Lang ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(City::className(), ['id' => 'parent_id']);
    }
}
