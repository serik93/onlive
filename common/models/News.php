<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use v0lume\yii2\metaTags\MetaTagBehavior;
use yii\behaviors\BlameableBehavior;
use trntv\filekit\behaviors\UploadBehavior;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $title_ru
 * @property string $desc_en
 * @property string $desc_ru
 * @property integer $author_id
 * @property integer $updater_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $slug
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $thumbnail;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
                
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => 'updater_id',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ],
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'immutable' => true
            ],
            'MetaTag' => [
                'class' => MetaTagBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
                'languages' => ['en', 'ru'],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'title_ru', 'desc_en', 'desc_ru', 'short_desc_en', 'short_desc_ru'], 'required'],
            [['desc_en', 'desc_ru', 'short_desc_en', 'short_desc_ru'], 'string'],
            [['author_id', 'updater_id'], 'integer'],
            [['created_at', 'updated_at', 'thumbnail'], 'safe'],
            [['title', 'title_ru'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'title' => 'Название (en)',
            'title_ru' => Yii::t('common', 'Title Ru'),
            'desc_en' => Yii::t('common', 'Desc En'),
            'short_desc_en' => 'Короткое описание на анг.',
            'desc_ru' => Yii::t('common', 'Desc Ru'),
            'short_desc_ru' => 'Короткое описание на русском',
            'author_id' => Yii::t('common', 'Author ID'),
            'updater_id' => Yii::t('common', 'Updater ID'),
            'created_at' => Yii::t('common', 'Created At'),
            'updated_at' => Yii::t('common', 'Updated At'),
            'slug' => Yii::t('common', 'Slug'),
            'thumbnail_base_url' => 'Thumbnail Base Url',
            'thumbnail_path' => 'Thumbnail Path',
            'thumbnail' => 'Картинка'
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\NewsQuery(get_called_class());
    }

    public function getPosThirdRow($value) {
        $pos = 0;
        for ($i = 0; $i < 3; $i++) {
            //$pos = strpos($value, '<li>', ($pos+1));
            $pos = $this->strpos_array($value, ['<li>','<p>'], ($pos+4));
            //var_dump($pos);
        }
        return $pos;
    }

    private function strpos_array($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
                $res = strpos($haystack, $needle, $offset);
                if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
    }
}
