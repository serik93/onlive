<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "camera_content".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $lang_id
 * @property string $name
 * @property string $address
 *
 * @property Camera $parent
 */
class CameraContentRu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'camera_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'lang_id'], 'integer'],
            [['name', 'address'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Camera::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'lang_id' => Yii::t('app', 'Lang ID'),
            'address' => 'Адрес(рус)',
            'description' => 'Описание',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Camera::className(), ['id' => 'parent_id']);
    }
}
