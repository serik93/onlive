<?php

namespace common\models;

use Yii;

use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

use yii\db\Expression;
use yii\db\ActiveRecord;

use common\models\Webcam;
/**
 * This is the model class for table "rating".
 *
 * @property integer $id
 * @property integer $star
 * @property integer $webcam_id
 * @property datetime $created_at
 * @property datetime $updated_at
 *
 * @property Webcam $webcam
 */
class Rating extends ActiveRecord
{
    /**
        count how many stars user gave particularly for camera
    */
    public $cnt;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rating';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            // [
            //     'class' => BlameableBehavior::className(),
            //     'createdByAttribute' => 'uid',
            //     'updatedByAttribute' => 'updater_id',
            //     'attributes' => [
            //         ActiveRecord::EVENT_BEFORE_VALIDATE => ['uid', 'updater_id']
            //     ]
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['star','webcam_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['webcam_id'], 'exist', 'skipOnError' => true, 'targetClass' => Webcam::className(), 'targetAttribute' => ['webcam_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'star' => Yii::t('frontend', 'Star'),
            'webcam_id' => 'Webcam',
            'created_at' => Yii::t('frontend', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebcam()
    {
        return $this->hasOne(Webcam::className(), ['id' => 'webcam_id']);
    }

    /** 
    * @return \yii\db\ActiveQuery
    */ 
    // public function getUpdater()
    // {
    //    return $this->hasOne(User::className(), ['id' => 'updater_id']); 
    // }
}
