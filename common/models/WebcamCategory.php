<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "webcam_category".
 *
 * @property integer $id
 * @property string $name_en
 * @property string $name_ru
 *
 * @property Webcam[] $webcams
 */
class WebcamCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'webcam_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ru'], 'required'],
            [['name_en', 'name_ru', 'slug'], 'string', 'max' => 255],
            [['slug'], 'safe'],
            [['slug'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name_en',
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
                'immutable' => false,
            ],
            // 'MetaTag' => [
            //     'class' => MetaTagBehavior::className(),
            // ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_en' => Yii::t('app', 'Name En'),
            'name_ru' => Yii::t('app', 'Name Ru'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebcams()
    {
        return $this->hasMany(Webcam::className(), ['category_id' => 'id']);
    }

    public function getName()
    {
        return Yii::$app->language == 'ru-RU' ? $this->name_ru : $this->name_en;
    }
}
