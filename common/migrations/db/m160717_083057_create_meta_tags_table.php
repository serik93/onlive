<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation for table `meta_tags`.
 */
class m160717_083057_create_meta_tags_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
       $this->createTable('{{%meta_tags}}', [
            'id' => Schema::TYPE_PK,
            'model' => Schema::TYPE_STRING . ' NOT NULL',
            'model_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'keywords' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'time_update' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
        ]);

        $this->createIndex('object', '{{%meta_tags}}', ['model', 'model_id'], true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('meta_tags');
    }
}
