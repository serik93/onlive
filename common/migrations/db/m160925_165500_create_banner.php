<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation for table `meta_tags`.
 */
class m160925_165500_create_banner extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%banner}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(512)->notNull(),
            'slot' => $this->string(512)->notNull(),
            'body' => $this->text()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%banner}}');
    }
}
