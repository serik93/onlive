<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation for table `meta_tags`.
 */
class m160925_164100_add_language_columns_to_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%page}}', 'body_ru', $this->text());
        $this->addColumn('{{%page}}', 'title_ru', $this->string(512));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%page}}', 'body_ru');
        $this->dropColumn('{{%page}}', 'title_ru');
    }
}
