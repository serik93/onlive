<?php

namespace backend\controllers;

use Yii;
use common\models\Camera;
use common\models\CameraSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CameraController implements the CRUD actions for Camera model.
 */
class CameraController extends Controller
{
    public function actions() {
        return [
            'index' => 'pjhl\multilanguage\actions\ActionIndex',
            'create' => 'pjhl\multilanguage\actions\ActionCreate',
            'view' => 'pjhl\multilanguage\actions\ActionView',
            'update' => 'pjhl\multilanguage\actions\ActionUpdate',
            'delete' => 'pjhl\multilanguage\actions\ActionDelete',
            'deleteContent' => 'pjhl\multilanguage\actions\ActionDeleteContent',
        ];
    }
    public static function mlConf($key = null) {
        $data = [

            'model' => 'common\models\Camera',

            'contentModel' => 'common\models\CameraContent',

            'contentModelRu' => 'common\models\CameraContentRu',

            'searchModel' => 'common\models\CameraSearch',
        ];
        return ($key!==null)
                ? $data[$key]
                : $data;
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Camera models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CameraSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Camera model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Camera model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Camera();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Camera model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Camera model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Camera model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Camera the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        // if (($model = Camera::findOne($id)) !== null) {
        //     return $model;
        // } else {
        //     throw new NotFoundHttpException('The requested page does not exist.');
        // }
         $query = Camera::find($id)
                ->where(['id'=>$id]);
        if ($langId!==null) {
            $query->with([
                'content' => function (\yii\db\ActiveQuery $query) use ($langId) {
                    $query->andWhere(['lang_id' => $langId]);
                }
            ]);
        } else {
            $query->with('content');
        }
        $model = $query->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested row does not exist.');
        }
    }
}
