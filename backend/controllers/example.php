<?php

// @example 

// Define class settings
$cfg['snapshot'] = array(
 	'program' => '/usr/bin/mplayer',
 	'outdir' => '/tmp',
 	'progressive' => False,
 	'baseline' => True,
 	'optimize' => 100,
 	'smooth' => 0,
 	'quality' => 90,
 	'frames' => 5,
 	'delete' => False
);

// Path to the input file
(string) $file = "argonnerwald.flv";

// Load the Snapshot class
include_once( 'snapshot.class.php' );

try {

	// Create the $img object
	(object) $img = new Snapshot( $cfg['snapshot'] );

	// Extract a jpeg image from defined video file
	if ( $img->extract( $file ) ) {
		if ( is_file($img->getOutfile()) ) {
			// If you have set the parameter delete to Fale, you have
			// to delete the extracted file manually with the remove() method.
			echo $file." was successfuly extracted to <a href=\"file://".$img->getOutfile()."\">".$img->getOutfile()."</a><br/>\n";
		} else {
			echo "Sorry, but I can't find any output file!";
		}
	} else {
		echo "Can't extract an image from the movie [".$file."]!";
	}

	// You also can stream the image (send image header)
	// if you set the second parameter to True
	//if ( !$img->extract( $file, True ) ) {
	//	echo "Can't extract ".$file;
	//}
 
	// You can remove the outfile manullay if $delete is False
	//if ( $cfg['snapshot']['delete'] === False ) {
	//	if ( !$img->remove($img->getOutfile()) )
	//		die( "Can't delete the output file [".$img->getOutfile()."]!" );
	//}

} catch ( Exception $error ) {
	echo "Error: ".$error->getMessage()."; on line ".$error->getLine()." in ".$error->getFile()."\n";
}

?>
