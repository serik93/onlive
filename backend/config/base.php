<?php
return [
    'id' => 'backend',
    'basePath' => dirname(__DIR__),
    'sourceLanguage'=>'ru_RU',
    'language'=>'ru_RU',
    'components' => [
        'urlManager' => require(__DIR__.'/_urlManager.php'),
        'frontendCache' => require(Yii::getAlias('@frontend/config/_cache.php'))
    ],
];
