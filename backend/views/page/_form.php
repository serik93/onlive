<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#seo">SEO</a></li>
        <li><a data-toggle="tab" href="#main">MAIN</a></li>
    </ul>

    <div class="tab-content">
        <div id="seo" class="tab-pane fade in active">
            <?php echo \notgosu\yii2\modules\metaTag\widgets\metaTagForm\Widget::widget(['model' => $model]) ?>
        </div>
        <div id="main" class="tab-pane fade">
            <?php echo $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            
            <?php echo $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

            <?php 
                echo $form->field($model, 'body')->widget(mihaildev\ckeditor\CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
            ?>

            <?php 
                echo $form->field($model, 'body_ru')->widget(mihaildev\ckeditor\CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
            ?>

            <?php echo $form->field($model, 'view')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'status')->checkbox() ?>

            <div class="form-group">
                <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
