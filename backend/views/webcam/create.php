<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Webcam */

$this->title = Yii::t('common', 'Create {modelClass}', [
    'modelClass' => 'Webcam',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Webcams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="webcam-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'cities' => $cities,
    ]) ?>

</div>
