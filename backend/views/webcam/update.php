<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Webcam */

$this->title = Yii::t('common', 'Update {modelClass}: ', [
    'modelClass' => 'Webcam',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Webcams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('common', 'Update');
?>
<div class="webcam-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'cities' => $cities,
    ]) ?>

</div>
