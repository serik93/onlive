<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Webcam */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Webcams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="webcam-view">

    <p>
        <?php echo Html::a(Yii::t('common', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a(Yii::t('common', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('common', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'category_id',
            'slug',
            'title_en',
            'title_ru',
            'desc_ru:ntext',
            'desc_en:ntext',
            'city_id',
            'thumbnail_base_url:url',
            'thumbnail_path',
            'created_at',
            //'updated_at',
            'author_id',
        ],
    ]) ?>

</div>
