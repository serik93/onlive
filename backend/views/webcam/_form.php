<?php

use trntv\filekit\widget\Upload;
use trntv\yii\datetime\DateTimeWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use v0lume\yii2\metaTags\MetaTags;

/* @var $this yii\web\View */
/* @var $model common\models\Webcam */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="webcam-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#seo">SEO</a></li>
      <li><a data-toggle="tab" href="#main">MAIN</a></li>
      <li><a data-toggle="tab" href="#picture">Обложка</a></li>
    </ul>
    <div class="tab-content">
        <div id="seo" class="tab-pane fade in active">
            <?php
            // echo MetaTags::widget([
            //     'model' => $model,
            //     'form' => $form
            // ]);
            echo \notgosu\yii2\modules\metaTag\widgets\metaTagForm\Widget::widget(['model' => $model])
            ?>
        </div>
        <div id="main" class="tab-pane fade">
            
            <?php echo $form->field($model, 'url')->textArea(['row' => 6]) ?>

            <?php echo $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(
                    $categories,
                    'id',
                    'name_ru'
                ), ['prompt'=>'']) ?>

            <?php echo $form->field($model, 'city_id')->dropDownList(\yii\helpers\ArrayHelper::map(
                    $cities,
                    'id',
                    'name_ru'
                ), ['prompt'=>'']) ?>

            <?php echo $form->field($model, 'slug')
                ->hint(Yii::t('backend', 'If you\'ll leave this field empty, slug will be generated automatically'))
                ->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'address_en')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'address_ru')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'web_url')->textInput(['maxlength' => true]) ?>

            <?php 
                echo $form->field($model, 'desc_ru')->widget(mihaildev\ckeditor\CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
            ?>

            <?php 
                echo $form->field($model, 'desc_en')->widget(mihaildev\ckeditor\CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
            ?>
        </div>
        <div id="picture" class="tab-pane fade">
            <?php echo $form->field($model, 'thumbnail')->widget(
                Upload::className(),
                [
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 5000000, // 5 MiB
                ]);
            ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
