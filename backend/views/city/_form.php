<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use mihaildev\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\City */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#seo">SEO</a></li>
      <li><a data-toggle="tab" href="#main">MAIN</a></li>
    </ul>

    <div class="tab-content">
        <div id="seo" class="tab-pane fade in active">

    	<?php echo \notgosu\yii2\modules\metaTag\widgets\metaTagForm\Widget::widget(['model' => $model]) ?>

    	</div>
		
		<div id="main" class="tab-pane fade">
		    <?php echo $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

		    <?php echo $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

		    <?php 
			    echo $form->field($model, 'desc')->widget(CKEditor::className(),[
			        'editorOptions' => [
			            'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
			            'inline' => false, //по умолчанию false
			        ],
			    ]);
			?>

		    <?php 
			    echo $form->field($model, 'desc_ru')->widget(CKEditor::className(),[
			        'editorOptions' => [
			            'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
			            'inline' => false, //по умолчанию false
			        ],
			    ]);
		    ?>
		</div>
	</div>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
