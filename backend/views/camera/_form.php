<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use pjhl\multilanguage\LangHelper;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model backend\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="hidden">
        <?= $form->field($model, 'id')->hiddenInput() ?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?//= // $form->field($model->content, 'lang_id')
                //    ->dropDownList(ArrayHelper::map(LangHelper::languages(), 'id', 'name')
                //            , array('disabled'=>!$model->isNewRecord)) ?>
        </div>
    </div>

    <?= $form->field($model->content, 'address')->textInput() ?>

    <?= $form->field($model->contentRu, 'address')->textInput() ?>

    <?= $form->field($model->content, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>

    <?= $form->field($model->contentRu, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord 
                ? Yii::t('app', 'Create') 
                : Yii::t('app', 'Update'), ['class' => $model->isNewRecord 
                        ? 'btn btn-success' 
                        : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>