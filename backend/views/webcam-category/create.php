<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\WebcamCategory */

$this->title = Yii::t('common', 'Create {modelClass}', [
    'modelClass' => 'Webcam Category',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Webcam Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="webcam-category-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
