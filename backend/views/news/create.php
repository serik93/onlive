<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = Yii::t('common', 'Create {modelClass}', [
    'modelClass' => 'News',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
